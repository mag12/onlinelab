import React, { Component, useState, useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Navbar from "../components/navbar";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import image1 from "../Assets/Images/img1.jpg";
import image2 from "../Assets/Images/img2.jpg";
import image3 from "../Assets/Images/img3.jpg";
import image4 from "../Assets/Images/img4.jpg";
import "../Assets/Style/home.css";
import Card from "../components/card";
import Heart from "../Assets/Images/heart.jpg";
import Malaria from "../Assets/Images/malaria.jpg";
import Footer from '../components/footer'
import Button from "@material-ui/core/Button";
import { authetication } from '../App'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()
function Home(props) {
    const history = useHistory();
    let navlink;
    if(sessionStorage.getItem('name')===null){
      navlink= <Navbar login="Se connecter" signup="S'enregistrer" />
    }
    else{
      navlink= <Navbar login="Se déconnecter" />
    }
    const heart = () => {
      if(sessionStorage.getItem('name')===null){
        // sessionStorage.setItem('heart',"heart")
        // sessionStorage.removeItem('malaria')
        toast.info("Veuillez vous connecter pour diagnostiquer une maladie cardiaque", {
          position: toast.POSITION_TOP_RIGHT,
        });
      }
      else{
        let path = `/heart`; 
        history.push(path);
      }
    }
      
    const malaria = () => {
      if(sessionStorage.getItem('name')===null){
        // sessionStorage.setItem('malaria',"malaria")
        // sessionStorage.removeItem('heart')
        toast.info("Veuillez vous connecter pour diagnostiquer le paludisme", {
          position: toast.POSITION_TOP_RIGHT,
        });
      }
      else{
        let path = `/malaria`; 
        history.push(path);
      }
    }
  return (
    <div className="text-center">
    <div className="over center">
       {navlink}
        <div className="card center pw10" id="heart">
          <Card
            image={Heart}
            title="Maladies Cardiovasculaires"
            description="Diagnostic de maladies cardiovasculaires basé sur l'IA"
          />
          <Button size="large" color="primary" onClick={heart}>
          Diagnostic
        </Button>
        </div>
        <div className="card" id="malaria">
          <Card
            image={Malaria}
            title="Paludisme"
            description="Diagnostic du paludisme basé sur l'IA"
          />
           <Button size="large" color="primary" onClick={malaria}>
          Diagnostic
        </Button>

        </div>
        </div>
       <Footer />
       </div>
      

    
  );
}

export default Home;
