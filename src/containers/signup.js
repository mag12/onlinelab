import React, { Component, useState, useRef, useEffect } from "react";
import "../Assets/Style/login.css";
import { useHistory } from "react-router-dom";
import Logo from "../Assets/Images/logo3.png";
import FormLabel from "@material-ui/core/FormLabel";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import Alert from "../components/alert.js";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import firebase from "../firebase";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import Navbar from "../components/navbar";

toast.configure();
function Signup(props) {

  const [loading,setLoading] = useState(false)
  const [phone, setPhone] = useState("");
  const [newPass,setnPass] = useState("");
  const [fname, setFName] = useState("");
  const [lname, setLName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const inputProps = {
    step: 300,
  };

  useEffect(() => {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
        // other options
      }
    );
  });

  const pushto = () => {
    let path = `/login`;
    history.push(path);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true)
    if (
      phone === "" ||
      fname === "" ||
      lname === "" ||
      email === "" ||
      password == ""
    ) {
      setLoading(false)
      toast.warn("Veuillez remplir tout les champs !", {
        position: toast.POSITION_TOP_RIGHT,
      });
    } else if (password !== newPass){
      setLoading(false)
      toast.warn("Le mot de passe ne correspond pas!", {
        position: toast.POSITION_TOP_RIGHT,
      });
    } 
    else {
      setLoading(true)
      const recap = window.recaptchaVerifier;
      let number = "+" + phone;
      firebase
        .auth()
        .signInWithPhoneNumber(number, recap)
        .then(function (e) {
          setLoading(false)
          let code = prompt("Saisir le code de verification", "");
          setLoading(true)
          if (code == null) {
            setLoading(false)
            console.log("error");
            toast.error("Code invalide", {
              position: toast.POSITION_TOP_RIGHT,
            });
          }
          setLoading(false)
          e.confirm(code)
            .then(function (result) {
              console.log(result.user, "user");
              toast.success("Téléphone vérifié", {
                position: toast.POSITION_TOP_RIGHT,
              });
              const user = {
                firstName: fname,
                lastName: lname,
                phone: phone,
                email: email,
                password: password,
              };
              const options = {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                },
              };
              setLoading(true)
              axios
                .post("https://onlinelab.herokuapp.com/users/register", user, options)
                .then((response) => {
                  setLoading(false)
                  if (response.data.result == "User already exists") {
                    toast.error("L'utilisateur existe déjà", {
                      position: toast.POSITION_TOP_RIGHT,
                    });
                  } else {
                    props.history.push({
                      pathname: "/login",
                    });
                  }
                }).catch((error) => {
                  console.log(error);
                });
            })
            .catch((e) => {
              console.log(e);
            });
        });
    }
  };

  return (
    <div>
      <Navbar home="Home" />
      <div className="login-logo">
        <div className="logo">
          <h1
            style={{
              position: "absolute",
              top: "25%",
              left: "20%",
              color: "white",
              fontSize: "70px",
              fontWeight: "bold",
              fontFamily: "Sansita Swashed",
            }}
          >
            OnlineLab
          </h1>
        </div>
      </div>
      <div className="login-form">
        <h3
        >
          S'ENREGISTRER
        </h3>
        <form>
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"45px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Nom
          </FormLabel>
          <TextField className="form9"
            autoFocus={true}
            id="fname"
            size="medium"
            type="text"
            helperText=""
            defaultValue={fname}
            onChange={(e) => setFName(e.target.value)}
            inputProps={inputProps}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"65px",
              marginLeft:"30px",
              fontSize: "30px",
              width: 300,
            }}
          />
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"135px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Prenom
          </FormLabel>
          <TextField className="form9"
            id="fname"
            size="medium"
            type="text"
            helperText=""
            defaultValue={lname}
            onChange={(e) => setLName(e.target.value)}
            inputProps={inputProps}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"165px",
              marginLeft:"30px",
              fontSize: "30px",
              width: 300,
            }}
          />
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"225px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Numero de Telephone
          </FormLabel>
          <PhoneInput className="form9" 
            country={"ga"}
            value={phone}
            onChange={(phone) => setPhone(phone)}
            autoFocus={true}
            inputStyle={{ width: "300px" }}
            id="number"
            size="medium"
            type="number"
            enableSearch={true}
            placeholder=""
            disableSearchIcon={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"265px",
              marginLeft:"30px",
              fontSize: "10px",
              width: 300,
            }}
          />
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"325px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Email
          </FormLabel>
          <TextField className="form9"
            id="email"
            size="medium"
            type="email"
            helperText="mag@gmail.com"
            inputProps={inputProps}
            defaultValue={email}
            onChange={(e) => setEmail(e.target.value)}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"345px",
              marginLeft:"30px",
              fontSize: "30px",
              width: 300,
            }}
          />
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"425px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Mot de passe
          </FormLabel>
          <TextField className="form9"
            id="password"
            size="medium"
            type="password"
            helperText=""
            inputProps={inputProps}
            defaultValue={password}
            onChange={(e) => setPassword(e.target.value)}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"445px",
              marginLeft:"30px",
              fontSize: "30px",
              width: 300,
            }}
          />
          <FormLabel className="form9"
            filled={true}
            focused={true}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"505px",
              marginLeft:"30px",
              fontSize: "20px",
            }}
          >
            Coonfirmation du mot de passe
          </FormLabel>
          <TextField className="form9"
            id="password"
            size="medium"
            type="password"
            helperText=""
            inputProps={inputProps}
            defaultValue={newPass}
            onChange={(e) => setnPass(e.target.value)}
            style={{
              color: "black",
              position: "absolute",
              marginTop:"525px",
              marginLeft:"30px",
              fontSize: "30px",
              width: 300,
            }}
          />

          <Fab className="form8"
            id="recaptcha-container"
            variant="extended"
            color="primary"
            onClick={handleSubmit}
            disabled={loading}
            style={{
              margin: "5px",
              width: 200,
              fontSize: "16px",
              color: "white",
              position: "absolute",
              marginTop:"585px",
              marginLeft:"30px",
            }}
          >
             {loading && <i className="fa fa-cog fa-spin"></i>}
             {loading && <span> En cours </span>}
             {!loading && <span> ENREGISTRER </span>}
          </Fab>
          <Fab className="form8"
            variant="extended"
            color="primary"
            onClick={pushto} 
            style={{
              margin: "5px",
              width: 200,
              fontSize: "16px",
              color: "white",
              position: "absolute",
              marginTop:"645px",
              marginLeft:"30px",
            }}
          >
            SE CONNECTER
          </Fab>
        </form>
      </div>
    </div>
  );
}

export default Signup;
